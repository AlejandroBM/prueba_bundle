package com.prueba.prueba_bundle;

import android.app.Activity;
import android.os.Bundle;
import android.view.TextureView;
import android.widget.TextView;

public class RecibeB extends Activity{
	
	public TextView imprimiendoText;
	public TextView imprimiendoText2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recibe_bundle);
		
		imprimiendoText = (TextView) findViewById (R.id.textView1);
		imprimiendoText2 = (TextView) findViewById(R.id.textView2);
		
		Bundle b = new Bundle();
		
		b = getIntent().getBundleExtra("envio_bundle");
		
		imprimiendoText.setText("Dato0: " + b.getString("my_data"));
		
	}

}
